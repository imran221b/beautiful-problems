//how promises work

var asyncAdd = (a,b) => {
  return new Promise((resolve, reject) => {
    setTimeout( () => {
      if(typeof a === 'number' && typeof b === 'number')
      {
        resolve(a+b);

      }
      else {
        reject('arguments must be numbers');

      }

    }, 1500)
  });
};


asyncAdd(85, 45).then((res)=>{
  console.log('result: ', res);
  return asyncAdd(res, 34); // chaining two promise functions
}).then((res)=> {
  console.log('should be something like ', res);

}).catch((errorMessage) => {
  console.log(errorMessage);
});


// var somePromise = new Promise((resolve, reject) => {
//   setTimeout(() => {
//   //  resolve('nothin');
//     //resolve('hey. it worked');
//   reject('unable to fulfill promise.');
//   }, 2500);
// });
//
// somePromise.then((message) => {
//   console.log('Success: ', message);
// }, (errorMessage) => {
//   console.log('Error: ', errorMessage);
// });
