const request = require('request');

var getWeather = (Lat, Lng, callback) => {
  request({
    url: `https://api.darksky.net/forecast/7fdc112b998dad9b4d218408f66b155b/${Lat},${Lng}`,
    json: true
  }, (error, response, body) => {
    if(error)
    {
      callback('Unable to connect to servers');
    }
    else if(!error && response.statusCode === 200)
    {
      callback(undefined,{
        temperature: body.currently.temperature,
        apparentTemperature: body.currently.apparentTemperature
      });
    }
    else {
      callback('unable to forecast io server');
    }
  })


}


module.exports.getWeather = getWeather;
