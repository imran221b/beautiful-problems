const fs = require('fs');
const os = require('os');
const _= require('lodash'); // requires 3rd party module
const yargs = require('yargs');

const titleOption = {
  describe: 'Title of Note',
  demand: true,
  alias: 't'

};
const bodyOption = {
    describe: 'Body of Note',
    demand: true,
    alias: 'b'
};
const argv = yargs
.command('add', 'ADD a new Note', {
  title:titleOption,
  body:bodyOption


})
.command('List', 'List all notes')
.command('read', 'Read a note', {
  title:titleOption

})
.command('remove', 'Remove a note', {
  title:titleOption
})
.help()
.argv;

const notes = require('./notes.js'); //requires the notes.js
                                     // file that you created


var command = argv._[0];


if(command === 'add')
{
  //console.log('Adding new note');
  var note = notes.addNote(argv.title, argv.body);
  if(note)
  {
    console.log(`Note Created`);
    notes.logNote(note);

  }
  else {
    console.log('There is  a duplicate');

  }
}
else if(command === 'list')
{
  //console.log("listing all notes");
  var allNotes = notes.getAll();
  console.log(`Printing ${allNotes.length}note(s)`);
  allNotes.forEach((note) => notes.logNote(note));
}
else if(command == 'remove')
{
  var check = notes.removeNote(argv.title);
  var message = check ? 'Note was removed' : 'Note not found';
  console.log(message);
}
else if(command == 'read')
{
  var note = notes.getNote(argv.title);
//  var message = notes ? 'here is your note ' + note.title +' '+ note.body : 'note not found';
  if(note)
  {
    console.log('note found');
    notes.logNote(note);

  }
  else {
    console.log('note not found');
  }
}
else
{
  console.log('command not recognized');
}
