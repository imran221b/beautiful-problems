/*var obj = {
  name: 'Nayeem'
};

var stringObj = JSON.stringify(obj);

console.log(typeof stringObj);
console.log(stringObj);
*/

/*var personString = '{"name":"Nayeem", "age": 23}';
var person = JSON.parse(personString);
console.log(typeof person);

*/

const fs = require('fs'); //to read a file;module;
var originalNote = {
  title: 'Some title',
  body: 'Some body'
};
var originalNoteString = JSON.stringify(originalNote); // originalNote is an object.
//JSON.stringify is taking orginalNote object and converting it to string.

fs.writeFileSync('notes.json', originalNoteString); // writing that string onto notes.json file
//which was automatically created in playground folder

var noteString = fs.readFileSync('notes.json');
var note = JSON.parse(noteString); // JSON.parse takes string and converts it back to regular
//java script object or array
console.log(typeof note);
console.log(note.title);
