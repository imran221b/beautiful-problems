var square = (x) => x*x;

console.log(square(9));

var user = {
  name: 'Nayeem',
  sayHi:() => {
    console.log(`hi.. i'm ${this.name}`);
  },
  sayHiAlt () {
    console.log(arguments);
    console.log(`hi.. i'm ${this.name}`);
  }
};

user.sayHiAlt(1,2,3);

user.sayHiAlt();
