const fs = require('fs');
//fetchNotes and saveNotes is a function that we are going to reesuse
var fetchNotes = () => {

	try{
		var notesString = fs.readFileSync('notes-data.json');
		return JSON.parse(notesString);
	}catch(e){
		return []; // if there is no file catch fun will return an empty array

	}

};

var saveNotes = (notes) => {
	fs.writeFileSync('notes-data.json', JSON.stringify(notes));

};


var addNote = (title, body) => {
	var notes = fetchNotes();
	var note = {
		title,
		body
	};

	var duplicateNotes = notes.filter((note) => note.title === title);

	if(duplicateNotes.length === 0)
	{
		notes.push(note);
		saveNotes(notes);
		return note;
	}

};




var getAll = () => {
	return fetchNotes();
};

var getNote = (title) => {
	var notes = fetchNotes();
	var readNotes = notes.filter((note) => note.title === title);
	return readNotes[0];
};

var removeNote = (title) =>{
	//fetch notes
	var notes = fetchNotes();
	//fileter notes, remvoing the one wih title of argument
  var keepNotes = notes.filter((note) => note.title !== title);
	//save new notes array
	 saveNotes(keepNotes);

	 return notes.length !== keepNotes.length;

};

var logNote = (note) =>{
	//debugger;
	console.log(`--`);
	console.log(`Title: ${note.title}`);
	console.log(`Body: ${note.body}`);

};

module.exports = {
	addNote,
	getAll,
//	readAll,
  logNote,
	getNote,
	removeNote
};
