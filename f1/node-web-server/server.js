const express = require('express');
const hbs = require('hbs');
const fs = require('fs');
const port = process.env.PORT || 3000;


// In the words of Heroku themselves:
//
//     Heroku apps expect the app directory structure at the root of the repository. If your app is inside a subdirectory in your repository, it won’t run when pushed to Heroku.
//
// This is obviously a problem if you've already created your project structure and don't particularly feel it's ok that Heroku dictate you change it.
//
// So..to fix it, we use the new(ish) git subtree module which was committed to the mainline git branch in April 2012. This basically allows us to push just a subdirectory of your repo to heroku during deployment.
//
// Given this project structure:
//
// --/
// ----/.git
// ----/build
// ----/docs
// ----/pythonapp
// ----/----/requirements.txt
//
// From the root of your repo, you can run the following command:
//
// git subtree push --prefix pythonapp heroku master
//
// This will push just the pythonapp folder to the remote.
//
// If you wish to read more about the subtree module, this article is a great start as it's from the author of the module itself.
//

//ulitmately use this {{{}git subtree push --prefix f1/node-web-server/ heroku master}}

var app = express();

hbs.registerPartials(__dirname + '/views/partials');
app.set('view engine', 'hbs');


app.use((req, res, next) => {       // rendering like template handleabar to see the
                                    //common things between middleware and handlebar
  var now = new Date().toString();
  var log = `${now}: ${req.method} ${req.url}`;
  console.log(log);
  fs.appendFile('server.log', log + '\n', (err) => {
    if(err)
    {
      console.log('Unable ot append to server.log.');
    }
  });

  next();
});


// app.use((req, res, next) => {
//   res.render('maintenance.hbs'); // after maintenacne everything will stop working . we didn't call next(). so it will not anything to be executed after this
// });

app.use(express.static(__dirname + '/public'));

hbs.registerHelper('getCurrentYear', () => {
  return new Date().getFullYear();

});

hbs.registerHelper('screamIt', (text) => {
  return text.toUpperCase();
});



app.get('/', (req, res) => {
  //res.send('<h1>hello express!</h1>');

  res.render('home.hbs', {
    pageTitle: 'Home Page',
    welcomeMessage: 'Welcome to my page'
  });
});


app.get('/about', (req, res) => {
  res.render('about.hbs', {
    pageTitle: 'About Page'

  });

});

app.get('/projects', (req, res) => {
  res.render('projects.hbs', {
    pageTitle: 'Projects'
  });

});


app.get('/bad', (req, res) =>{
  res.send({
    name: 'Game of Thrones',
    season: 9,
    airdate: '14/04/2019'

  });
});

app.use(express.static(__dirname, + '/views/images'));

app.listen(port, () => {
  console.log(`Server is running on port ${port}`);
});
