//connecting to the database. mongo client(library) connects to mongo db

const MongoClient = require('mongodb').MongoClient;

const url = 'mongodb://localhost:27017';
const dbName = 'TodoApp';

(async function() {
  const client = new MongoClient(url,{ useNewUrlParser: true } );

  try{
    await client.connect();
    console.log("Connceted correctly to server");

    const db = client.db(dbName);

    // let r = await db.collection('Todos').insertOne({
    //   text: 'something to do',
    //   completed: false
    // }, (err, result) => {
    //   if(err)
    //   {
    //     return console.log('unable to insert todo', err);
    //
    //   }
    //   console.log(JSON.stringify(result.ops, undefined, 2));
    // });

    let m = await db.collection('Users').insertOne({
      name: 'Sifat Shishir',
      age: 22,
      location: "Bangladesh"
    }, (err, result) => {
      if(err)
      {
        return console.log('unable to insert users', err);

      }

      console.log(JSON.stringify(result.ops, undefined, 2));
    });


  } catch(err)
  {
    console.log(err.stack);
  }

client.close();


})();
