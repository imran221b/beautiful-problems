var mongoose = require('mongoose');

//Setting up Mongoose
mongoose.Promise = global.Promise;
//'mongodb://localhost:27017/TodoApp', { useNewUrlParser: true }
mongoose.connect(process.env.MONGODB_URI, { useNewUrlParser: true });
mongoose.set('useFindAndModify', false);

module.exports = {mongoose};
