require('./config/config');
// mongoose setUP has been mooved to mongoose.js... then using module.exports we are getting it here
// as a variable./. this is destructuring of variable


//This is configuration part
// express and body-perser has been installed just before the configuration

//3rd party modules.. has only name inside require
const _ = require('lodash');
const express = require('express');
const bodyParser = require('body-parser');
const {ObjectID} = require('mongodb');//mongodb is a library




//this are local imports... we have created this locally... thats why the urls inside
//require
var {mongoose} = require('./db/mongoose');
var {Todo} = require('./models/todo');
var {User} = require('./models/user')

//we just moved everything to its rightfull place. here above we are usign destructring
// varibale  ans setting upthe conncetion in a different way

//firing up express
var app = express();

const port = process.env.PORT; // setting up process.env.PORT variable for heroku
//middleware
app.use(bodyParser.json());

//POST /todos
//this todo puts data into database
 app.post('/todos', (req, res) => {
   console.log(req.body);
//creating an instance of mongoose model
   var todo = new Todo({
     text: req.body.text
   });

   //saving todo inside db
   todo.save().then((doc) => {
     res.send(doc);
   }, (e) => {
     res.status(400).send(e);
   });
 });

//this todo brings everything from the database
 app.get('/todos', (req, res) => {
   Todo.find().then((todos) => {
     res.send({todos});
   }, (e) => {
     res.status(400).send(e);
   });
 });



//now we are fetching variables which was passed via urls
// this GET /todos/id will be different from before because we will be quering for todo
//using urls

app.get('/todos/:id', (req, res) => {
  var id = req.params.id; //getting the id using params method

  //now validation.. we want to check if the id is valid.
  if(!ObjectID.isValid(id)) {
    return res.status(404).send();
  }

  //since it is a GET method. we will fetch from db  usign findById
  Todo.findById(id).then((todo) => {
    if(!todo) {
      return res.status(404).send();
    }
    res.send({todo});// sending back todo as object to ensure flexibilty

  }).catch((e) => {
     res.status(400).send();
  });

});

//this is setting up route.... now we are creating delete route

app.delete('/todos/:id', (req, res) => {

  //get the id
  var id = req.params.id;

  //validate the id-> not valid? return 404

  if(!ObjectID.isValid(id)) {
    return res.status(404).send(); // returning it because if id is invalid there is no need to run the code below
  }

  //remove todo by id
    //success
      //if nod doc, send 404
      // if doc, send doc back with 200
    //error
      //400 with empty body

  Todo.findByIdAndRemove(id).then((todo) => {
    if(!todo) {
      return res.status(404).send();
    }
    res.send({todo});
  }).catch((e) => {
    res.status(400).send();
  });
 });



//http patch method.. patch is for updating a resource


app.patch('/todos/:id', (req, res) => {
  var id = req.params.id;
  //body variable , it has the sub-set of things the user passed to us
  //we don't want the user to be able to update anything they choose. e.g: _id
  var body = _.pick(req.body, ['text', 'completed']);

  if (!ObjectID.isValid(id)) {
    return res.status(404).send();
  }

  if (_.isBoolean(body.completed) && body.completed) {
    body.completedAt = new Date().getTime();
  } else {
    body.completed = false;
    body.completedAt = null;
  }

  Todo.findByIdAndUpdate(id, {$set: body}, {new: true}).then((todo) => {
    if (!todo) {
      return res.status(404).send();
    }

    res.send({todo});
  }).catch((e) => {
    res.status(400).send();
  })
});




//firing up express 2
app.listen(port, () => {
  console.log(`Started up at port ${port}`); //here is the port variable for heroku
});


module.exports = {app};







//----------------------------------EVERYTHING BELOW IS PRACTICE CODE----
//creating a new instance
// var newTodo = new Todo({
//   text: 'Catch fish'
// });
// //saving it to the db
// newTodo.save().then((doc) => {
//   console.log('Saved todo', doc);
// }, (e) => {
//   console.log('Unable to save todo');
// });

// var otherTodo = new Todo({
//   text: '  Edit this video    '
// });
//
// otherTodo.save().then((doc) => {
//   console.log(JSON.stringify(doc, undefined, 2));
// }, (e) => {
//   console.log('Unable to save', e);
//
// });


//
// var newuser = new User({
//   email: 'damonmayeehamm@gmail.com     '
// });
//
// newuser.save().then((doc) => {
//   console.log(JSON.stringify(doc, undefined, 2));
//
// }, (e) => {
//   console.log('Unable to save', e);
//
// });
