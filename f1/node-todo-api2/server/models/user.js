//new User model
//email - require + trim + set type + set min length of 1
//
// We are going to use ORM. THis is the model of an instance which will reside in
// collection/DatabaseTable


var mongoose = require('mongoose');

var User = mongoose.model('User', {
  email: {
    type: String,
    required: true,
    trim: true,
    minlength: 1
  }
});

module.exports = {User};
