//for todo database.... we are separting the model and putting it here.
//we will simply call


//
// We are going to use ORM. THis is the model of an instance which will reside in
// collection/DatabaseTable
var mongoose = require('mongoose');
var Todo = mongoose.model('Todo', {
  text: {
    type: String,
    required: true,
    minlength: 1,
    trim: true
  },
  completed: {
    type: Boolean,
    default: false
  },
  completedAt:{
    type: Number,
    default: null
  }
});

module.exports = {Todo};
