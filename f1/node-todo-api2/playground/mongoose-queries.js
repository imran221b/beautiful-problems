//load mongoose.js from db folder
//load todo.js from models
const {ObjectID} = require('mongodb');// from mongodb we are pulling ObjectID and using it
                                      //as a variable

const {mongoose} = require('./../server/db/mongoose');
const {Todo} = require('./../server/models/todo');
const {User} = require('./../server/models/user');

// var id = '5c9d1e148ce5f62020774b56'; // will be used for quering
// this part is for checking if the id is valid or not;
// if(!ObjectID.isValid(id)) {
//   console.log('ID not valid');
// }

//to get all the todos

// Todo.find({
//   _id: id
// }).then((todos) => {
//   console.log('Todos', todos);
// });
//
// Todo.findOne({
//  _id: id
// }).then((todo) => {
//   console.log('Todo', todo);
// });
//
// Todo.findById(id).then((todo) => {
//   if(!todo) {
//     return console.log('Id not found');
//   }
//   console.log('Todo by id', todo);
// }).catch((e) => console.log(e));


var id = '5c9cd52b23862928b4267ed4';
User.findById(id).then((user) => {
  if(!user) {
    return console.log('Id not found');
  }
  console.log(JSON.stringify(user, undefined, 2));//pretty printing technique
}).catch((e) => console.log(e));
