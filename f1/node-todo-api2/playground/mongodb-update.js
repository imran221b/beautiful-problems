// const MongoClient = require('mongodb').MongoClient;
const {MongoClient, ObjectID} = require('mongodb');


MongoClient.connect('mongodb://localhost:27017/TodoApp', (err, db) => {
  if(err){
    return console.log('unable to connect mongodb server');
  }
  console.log('connected to mongodb server');

  //update method findOneAndUpdate

  // db.collection('Todos').findOneAndUpdate({
  //   _id: new ObjectID('5c9bcc27c14b562524a677f7')
  // }, {
  //   $set: {
  //     completed: true
  //   }
  // }, {
  //   returnOriginal:false
  // }).then((result) => {
  //   console.log(result);
  // });

  //this one is for changing name attribute

  // db.collection('Users').findOneAndUpdate({
  //   _id: new ObjectID('5c9bcdc7a7f21d0c70a35b2a')
  // }, {
  //   $set: {
  //     name: 'Homo'
  //   }
  // }, {
  //   returnOriginal: false
  // }).then((result) => {
  //   console.log(result);
  // });
// USING $inc (increment) operator on Users db to increment age attribute
//this is the documentaion
//{
//   _id: 1,
//   sku: "abc123",
//   quantity: 10,
//   metrics: {
//     orders: 2,
//     ratings: 3.5
//   }
// }
// db.products.update(
//    { sku: "abc123" },
//    { $inc: { quantity: -2, "metrics.orders": 1 } }
// )
  db.collection('Users').findOneAndUpdate({
    _id: new ObjectID('5c9bcdc7a7f21d0c70a35b2a')
  }, {
    $inc: {
      age: 2
    }
  }, {
    returnOriginal: false
  }).then((result) => {
    console.log(result);
  });
//  db.close();
});
