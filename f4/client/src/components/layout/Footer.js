// //Reacts functional component
// //Functional Components are for display, doesn't have lifecyle or state
import React from 'react'

export default () => {
  return (
    <footer className= "bg-dark text-white mt-5 p-4 text-center">
        Copyright &copy; {new Date().getFullYear()} BeautifulProgram
    </footer>
  )
}
