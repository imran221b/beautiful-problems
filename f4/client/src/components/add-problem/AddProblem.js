import React, { Component } from 'react';
import { Link, withRouter } from 'react-router-dom';
import TextFieldGroup from '../common/TextFieldGroup';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { addProblem } from '../../actions/profileActions';
import axios from 'axios';

class AddProblem extends Component {
  constructor(props) {
    super(props);
    this.state = {
      pname: '',
      onlinejudge: '',
      contestname: '',
      url: '',
      statement: '',
      tags: '',
      rating: '',
      date: '',
      errors: {}
    };

    this.onChange = this.onChange.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
    this.onCheck = this.onCheck.bind(this);
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.errors) {
      this.setState({ errors: nextProps.errors });
    }
  }

  onSubmit(e) {
    e.preventDefault();

    const probData = {
      pname: this.state.pname,
      onlinejudge: this.state.onlinejudge,
      contestname: this.state.contestname,
      url: this.state.url,
      Statement: this.state.Statement,
      tags: this.state.tags,
      rating: this.state.rating,
      date: this.state.date

    };

    this.props.addProblem(probData, this.props.history);


        axios
          .post('/api/problem', probData )
          .then(res => console.log(res.data))
          .catch(err => console.log(err.response.data));

  }

  onChange(e) {
    this.setState({ [e.target.name]: e.target.value });
  }

  onCheck(e) {
    this.setState({
      disabled: !this.state.disabled,
      current: !this.state.current
    });

  }

  render() {
    const { errors } = this.state;

    return (
      <div className="add-education">
        <div className="container">
          <div className="row">
            <div className="col-md-8 m-auto">
              <Link to="/dashboard" className="btn btn-light">
                Go Back
              </Link>
              <h1 className="display-4 text-center">Add Problem</h1>
              <p className="lead text-center">
                Add any school, bootcamp etc that you have attended
              </p>
              <small className="d-block pb-3">* = required fields</small>
              <form onSubmit={this.onSubmit}>
                <TextFieldGroup
                  placeholder="* Problem Name"
                  name="pname"
                  value={this.state.pname}
                  onChange={this.onChange}
                  error={errors.pname}
                />
                <TextFieldGroup
                  placeholder="* Online Judge"
                  name="onlinejudge"
                  value={this.state.onlinejudge}
                  onChange={this.onChange}
                  error={errors.onlinejudge}
                />
                <TextFieldGroup
                  placeholder="* URL link"
                  name="url"
                  value={this.state.url}
                  onChange={this.onChange}
                  error={errors.url}
                />

                <TextFieldGroup
                  placeholder= "Statement"
                  name="statement"
                  value={this.state.statement}
                  onChange={this.onChange}
                  error={errors.statement}
                />

                <TextFieldGroup
                  placeholder= "Tags"
                  name="tags"
                  value={this.state.tags}
                  onChange={this.onChange}
                  error={errors.tags}
                />

                <TextFieldGroup
                  placeholder="* Rating"
                  name="rating"
                  value={this.state.rating}
                  onChange={this.onChange}
                  error={errors.rating}
                  info="Tell us about the program that you were in"
                />
                <input
                  type="submit"
                  value="Submit"
                  className="btn btn-info btn-block mt-4"
                />
              </form>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

AddProblem.propTypes = {
  addProblem: PropTypes.func.isRequired,
  profile: PropTypes.object.isRequired,
  errors: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
  profile: state.profile,
  errors: state.errors
});

export default connect(mapStateToProps, { addProblem })(
  withRouter(AddProblem)
);
