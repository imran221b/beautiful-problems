const Validator = require("validator");
const isEmpty = require("./is-empty");

module.exports = function validateProblemInput(data) {
  let errors = {};

  data.pname = !isEmpty(data.pname) ? data.pname : "";
  data.onlinejudge = !isEmpty(data.onlinejudge) ? data.onlinejudge : "";
  data.url = !isEmpty(data.url) ? data.url : "";

  // if (Validator.isEmpty(data.panme)) {
  //   errors.pname = "Problem name is invalid";
  // }

  if (Validator.isEmpty(data.onlinejudge)) {
    errors.onlinejudge = "Online Judge is required";
  }

  if (Validator.isEmpty(data.url)) {
    errors.url = "url is required";
  }
  if(!isEmpty(data.url)) {
    if(!Validator.isURL(data.url)) {
      errors.url = 'Not a valid URL';
    }
  }

  return {
    errors,
    isValid: isEmpty(errors)
  };
};
