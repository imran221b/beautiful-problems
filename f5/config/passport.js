const JwtStrategy = require("passport-jwt").Strategy;
const ExtractJwt = require("passport-jwt").ExtractJwt;
const FacebookStrategy = require("passport-facebook").Strategy;
const GooglePlusTokenStratedy = require('passport-google-plus-token');
const mongoose = require("mongoose");
const User = mongoose.model("users");
const keys = require("../config/keys");
const configAuth = require("./auth");
const gravatar = require("gravatar");
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
const opts = {};
opts.jwtFromRequest = ExtractJwt.fromAuthHeaderAsBearerToken();
opts.secretOrKey = keys.secretOrKey;

module.exports = passport => {
  passport.use(
    new JwtStrategy(opts, (jwt_payload, done) => {
      User.findById(jwt_payload.id)
        .then(user => {
          if (user) {
            return done(null, user);
          }
          return done(null, false);
        })
        .catch(err => console.log(err));
    })
  );
//Google Oauth Strategy


  passport.use('googleToken', new GooglePlusTokenStratedy({
    clientID: '622079694154-kjsk5fa6pdl1l72q5l7j8dv6fc0pvorc.apps.googleusercontent.com',
    clientSecret: 'JT6USgSfG_Gt0QRXS3f-0VL3'
  }, function(accessToken, refreshToken, profile, done, req, res) {

      console.log('profile', profile);


      User.findOne({ email: profile.emails[0].value }).then(user => {
        if (user) {
          console.log('user', user);
          const password =  profile.id;
          bcrypt.compare(password, user.password).then(isMatch => {
            if (isMatch) {
              // User Matched
              console.log('password matched');
              const payload = { id: user.id, name: user.name, avatar: user.avatar }; // Create JWT Payload

              // Sign Token
              jwt.sign(
                payload,
                keys.secretOrKey,
                { expiresIn: 3600 },
                (err, token) => {
                  res.json({
                    success: true,
                    token: "Bearer " + token
                  });
                }
              );
            } else {
              errors.password = "Password incorrect";
              return res.status(400).json(errors);
            }
          }).catch(err => console.log(err));

        } else {
          const avatar = gravatar.url(profile.emails[0].value, {
            s: "200", // Size
            r: "pg", // Rating
            d: "mm" // Default
          });

          const newUser = new User({
            name: profile.name.givenName + ' ' + profile.name.familyName,
            email: profile.emails[0].value,
            avatar,
            password: profile.id
          });

          bcrypt.genSalt(10, (err, salt) => {
            bcrypt.hash(newUser.password, salt, (err, hash) => {
              if (err) throw err;
              newUser.password = hash;
              newUser
                .save()
                .then(user => res.json(user))
                .catch(err => console.log(err));
            });
          });
        }
      }).catch(err => console.log(err));

  }));

  passport.use(new FacebookStrategy({
      clientID: configAuth.facebookAuth.clientID,
      clientSecret: configAuth.facebookAuth.clientSecret,
      callbackURL: configAuth.facebookAuth.callbackURL
    },
    function(accessToken, refreshToken, profile, done) {
        process.nextTick(function(){
          User.findOne({'facebook.id': profile.id}, function(err, user){
              if(err)  {
                return done(err);
              }
              if(user) {
                return done(null, user);
              } else {
                var newUser = new User();
                newUser.facebook.id = profile.id;
                newUser.facebook.token = accessToken;
                newUser.facebook.name = profile.name.givenName + ' ' + profile.name.familyName;
                newUser.facebook.email = profile.emails[0].value;

                newUser.save(function(err) {
                  if(err) {
                    throw err;
                  }
                  return done (null, newUser);
                })
                console.log(profile);
              }

          });


        });

      }));
    };
