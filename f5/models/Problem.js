const mongoose = require("mongoose");
const Schema = mongoose.Schema;
mongoose.set("useFindAndModify", false);

//Creating Schema

const ProblemSchema = new Schema({
  user: {
    type: Schema.Types.ObjectId,
    ref: "users"
  },
  pname: {
    type: String,
    required: true
  },
  onlinejudge: {
    type: String,
    required: true
  },
  contestname: {
    type: String
  },
  url: {
    type: String,
    required: true
  },
  statement: {
    type: String
  },
  tags: {
    type: [String]
  },
  rating: {
    type: Number,
    required: true
  },
  date: {
    type: Date,
    default: Date.now
  }
});

module.exports = Problem = mongoose.model("problem", ProblemSchema);
//If rating has subcategories
// rating: [
//   {
//     problemDifficulty: {
//       type: Number,
//       required: true
//     },
//     implementationDifficulty: {
//       type: Number,
//       required: true
//     },
//     educationalValue: {
//       type: Number,
//       required: true
//     },
//     aestheticBeauty: {
//       type: Number,
//       required: true
//     }
//   }
// ]
