const express = require("express");
const router = express.Router();
const mongoose = require("mongoose");
const passport = require("passport");
// const gravatar = require("gravatar");
// const bcrypt = require("bcryptjs");
// const jwt = require("jsonwebtoken");
// const keys = require("../../config/keys");
// const passport = require("passport");

//Load Validation
const validateProblemInput = require('../../validation/problem');


//load problem and user
const Problem = require("../../models/Problem");
const User = require("../../models/User");
const Profile = require("../../models/Profile");


// @route   GET api/problem/abcd
// @desc    Testing Problem Page Setup
// @access  Public
router.get('/abcd', (req, res) => res.json({ msg: 'Problem PAGE'}) );

// @route   GET api/problem/
// @desc    getting all the problems rated by the user
// @access  private

router.get(
  "/problem",
  passport.authenticate("jwt", { session: false }),
  (req, res) => {
    const errors = {};

    Problem.findOne({ user: req.user.id })
      .populate("user", ["name", "avatar"])
      .then(problem => {
        if (!problem) {
          errors.noprofile = "There is no problem rated by the user";
          return res.status(404).json(errors);
        }
        res.json(problem);
      })
      .catch(err => res.status(404).json(err));
  }
);

// @route   GET api/problem/all
// @desc    Get all Problems
// @access  Public

router.get('/all', (req, res) => {
  const errors = {};
  Problem.find()
    .then(problems => {
      if(!problems) {
        errors.noproblem = 'There are no problems in the site';
        return res.status(404).json(errors);
      }
      res.json(problems);
    })
    .catch(err =>
      res.status(404).json({ problem: 'There are no problems in the site'})
    );
});


// @route   POST api/problem
// @desc    Create or Edit Problem
// @access  private

router.post(
  "/",
  passport.authenticate("jwt", { session: false }),
  (req, res) => {
    const { errors, isValid } = validateProblemInput(req.body);

    //check Validation
    if(!isValid) {
      //return any errors with 400 status
      return res.status(400).json(errors);
    }

    //Get fields
    const problemFields = {};
    problemFields.user = req.user.id;

    if(req.body.pname) problemFields.pname = req.body.pname;
    if(req.body.onlinejudge) problemFields.onlinejudge = req.body.onlinejudge;
    if(req.body.contestname) problemFields.contestname = req.body.contestname;
    if(req.body.url) problemFields.url = req.body.url;
    if(req.body.statement) problemFields.statement = req.body.statement;
    if(typeof req.body.tags !== 'undefined')  {
      problemFields.tags = req.body.tags.split(',');
    }
    if(req.body.rating) problemFields.rating = req.body.rating;
    if(req.body.date) problemFields.date = req.body.date;

    //for arraray objects like rating objects
    // if(req.body.problemDifficulty) problemFields.rating.problemDifficulty = req.body.problemDifficulty;
    // if(req.body.implementationDifficulty) problemFields.rating.implementationDifficulty = req.body.implementationDifficulty;
    // if(req.body.educationalValue) problemFields.rating.educationalValue = req.body.educationalValue;
    // if(req.body.aestheticBeauty) problemFields.rating.aestheticBeauty = req.body.aestheticBeauty;


        new Problem(problemFields).save().then(problem => res.json(problem));

      });












module.exports = router;
