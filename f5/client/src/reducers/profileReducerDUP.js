import {
  GET_PROFILE,
  GET_PROFILES,
  PROFILE_LOADING,
  CLEAR_CURRENT_PROFILE,
  PROBLEM_LOADING,
  GET_PROBLEMS
} from '../actions/types';

const initialState = {
  profile: null,
  profiles: null,
  problems: null,
  loading: false
};

export default function(state = initialState, action) {
  switch (action.type) {
    case PROFILE_LOADING:
      return {
        ...state,//current state
        loading: true
      };
      case PROBLEM_LOADING:
        return {
        ...state,//current stat
        loading: true
      };
    case GET_PROFILE:
      return {
        ...state,
        profile: action.payload,//action payload has the profile that being sent
        loading: false
      };
    case GET_PROFILES:
      return {
        ...state,
        profiles: action.payload,//action payload has the profile that being sent
        loading: false
      };
    case CLEAR_CURRENT_PROFILE:
      return {
        ...state,
        profile: null
      };
      case GET_PROBLEMS:
        return {
          ...state,
          problems: action.payload,//action payload has the profile that being sent
          loading: false
        };
    default:
      return state;
  }
}
