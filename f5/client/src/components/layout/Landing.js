// //Reacts class base Components
//
// import React, { Component } from 'react'
//
// export default class componentName extends Component {
//   render() {
//     return (
//       <div>
//
//       </div>
//
//     )
//   }
// }


import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { PropTypes } from 'prop-types';
import { connect } from 'react-redux';

class Landing extends Component {
  componentDidMount() {
    if (this.props.auth.isAuthenticated) {
      this.props.history.push('/dashboard');//rerouting to dashboard
    }
  }
  render() {
    return (
      <div className="landing">
        <div className="dark-overlay landing-inner text-light">
          <div className="container">
            <div className="row">
              <div className="col-md-12 text-center">
                <h1 className="display-3 mb-4">Beautiful Problem
                </h1>
                <p className="lead"> Give raitings to problems and see all the problems rated by othe users</p>
                <hr />
                <Link to="/register" className="btn btn-lg btn-info mr-2">Sign Up</Link>
                <Link to="/login" className="btn btn-lg btn-light">Login</Link>
              </div>
            </div>
          </div>
        </div>
      </div>

    )
  }
}

//creating landing proptyes
Landing.propTypes = {
  auth: PropTypes.object.isRequired
};
//setting map state to props
const mapStateToProps = state => ({
  auth: state.auth
});

export default connect(mapStateToProps)(Landing);
