import React, { Component } from 'react';
import PropTypes from 'prop-types';
//import { Link } from 'react-router-dom';
import isEmpty from '../../validation/is-empty';

class ProblemItem extends Component {
  render() {
    const { problem } = this.props;

    return (
      <div className="card card-body bg-light mb-3">
        <div className="row">
          <div className="col-2">

          </div>
           <div className="col-lg-6 col-md-4 col-8">
            <h3>{problem.pname}</h3>
            <p>
                {problem.onlinejudge} {problem.url}
            </p>
            <p>
            {isEmpty(problem.contestname) ? null : (<span> at {problem.onlinejudge} </span>) }
            </p>
            <p>
            </p>
           </div>
           <div className="col-md-4 d-none d-md-block">
            <h4>Tags</h4>
            <ul className="list-group">
              {problem.tags.slice(0,4).map((tag, index) => (
                <li key={index} className="list-group-item">
                  <i className="fa fa-check pr-1" />
                  {tag}
                </li>
              ))}
            </ul>
           </div>
        </div>
      </div>




    );
  }
}

ProblemItem.propTypes = {
  problem: PropTypes.object.isRequired
};

export default ProblemItem;
