import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import Spinner from '../common/Spinner';
import { getProblems } from '../../actions/profileActions';
import ProblemItem from './ProblemItem';

class Problems extends Component {

  componentDidMount() {
    this.props.getProblems();
  }

  render() {
    const  { problems } = this.props;
    console.log('this.props.problems', this.props.problems);

    let problemItems;
    if (problems === null ) {
      problemItems = <Spinner />;
    } else {
     // if (problems.length > 0) {
     //    problemItems = problems.map(problem => (
     //      <ProblemItem key={problem._id} problem={problem} />
     //    ));
     //  } else {
     //    problemItems = <h4>No profiles found...</h4>;
     // }

    }
   let problemIII = (req, res)  => {
      return res.json(console.log(problems[0]));

    }

    return (
      <div className="problems">
        <div className="container">
          <div className="row">
            <div className="col-md-12">
              <h1 className="display-4 text-center">User Problems</h1>
              <p className="lead text-center">
                Browse the problems
              </p>
              {problemItems}
              {problemIII}
            </div>
          </div>
        </div>
      </div>
    );
  }
}

Problems.propTypes = {
  getProblems: PropTypes.func.isRequired,
  problem: PropTypes.object

};

const mapStateToProps = state => ({
  problem: state.problem
});

 export default connect(mapStateToProps, { getProblems })(Problems);
